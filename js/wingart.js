$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// responsive project



// INCLUDE FUNCTION

var starRating 		= $(".example-css"),
	stripeRating 	= $(".example-1to10"),
	equalheight 	= $(".equalheight");

if(starRating.length || stripeRating.length){
  include("js/jquery.barrating.min.js");
}

if(equalheight.length){
  include("js/jquery.equalheights.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  //---- INICIALIZATION ----

  $('.btn_menu').on('click ontouchstart',function () {

    $('body').toggleClass('showMenu');
    
  });

  $('.form__wrapper').on('click ontouchstart', '.btn', function (e) {

    e.preventDefault();

    $('body').find('.form__item').toggleClass('show');
    
  });

  

    

  // widget tablet & mobile

    var wWidth = $(window).width();

    if(wWidth <=1100){

      $('.wa--widget__fixed--block-btn').on("click touchstart", function (e) {

        var $this = $(this),
            $widget = $('.wa--widget__fixed--block-content');

        if(!$widget.length) return;

        $widget.toggleClass('show');
        
      });

      $('.wa--widget__fixed--block-btn--close').on("click touchstart", function (e) {

        var $this = $(this),
            $widget = $('.wa--widget__fixed--block-content');

        if(!$widget.length) return;

        $widget.removeClass('show');
        
      });

      $('body').on("click touchstart", function(event){


        if ($(event.target).closest('.wa--widget__fixed--block-content').length || $(event.target).closest('.wa--widget__fixed--block-btn').length)  return;

        $('.wa--widget__fixed--block-content').removeClass("show");

        event.stopPropagation();


      })
    }
    // widget desktop
    else {
    	var $widget = $('.wa--widget__fixed--block-content'),
    		widgetTimeOutId;
    	$('.wa--widget__fixed--block-btn').on("mouseenter", function (e) {

    	if(widgetTimeOutId) clearTimeout(widgetTimeOutId);

	      var $this = $(this);

	      if(!$widget.length) return;

	      $widget.addClass('show');
	      
	    }).on("click touchstart", function (e) {

	      var $this = $(this);
	         

	      if(!$widget.length) return;

	      // $widget.removeClass('show');
	      
	      // $widget.on('mouseenter', function(e){
	      // 	if(!$widget.length) return;

	      // 	$widget.removeClass('show');
	      // })
	    }).on('mouseleave', function(e){
	    	if(!$widget.length) return;

	    	widgetTimeOutId = setTimeout(function(){
		    	$widget.removeClass('show');
		    }, 200);
	    });

	    if($widget.length) {
	    	$widget.on('mouseleave', function(e){
	    		var $this = $(this);
		    	widgetTimeOutId = setTimeout(function(){
			    	$this.removeClass('show');
			    }, 200);
		    })
		    .on('mouseenter', function(e){
		    	if(widgetTimeOutId) clearTimeout(widgetTimeOutId);
		    });
	    }
    }


  // ---------  Start Rating  ---------
  	if(starRating.length){
      $(starRating).each(function() {
        var currentInitVal = parseFloat($(this).data('current-rating'));

        $(this).barrating({
              theme: 'css-stars',
              showSelectedRating: false,
              readonly: true,
              initialRating: currentInitVal
          });
      })
  	}

  	if(stripeRating.length){
  		$(stripeRating).barrating('show', {
            theme: 'bars-1to10',
            readonly: true
        });
  	}
  //------------  EQUALHEIGHT  ------------
  	if(equalheight.length){
  		$(equalheight).equalHeights();
  	}


})